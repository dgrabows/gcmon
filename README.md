# gcmon

Investigating JVM garbage collection monitoring using JMX and Clojure.

## License

Copyright © 2012 Dan Grabowski

Distributed under the [Eclipse Public License](http://www.eclipse.org/legal/epl-v10.html).
