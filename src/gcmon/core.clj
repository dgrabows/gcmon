(ns gcmon.core
  (:require [clojure.java.jmx :as jmx])
  (:use [clojure.reflect :only [reflect]])
  (:import java.lang.management.ManagementFactory))

(defn -main
  "I don't do a whole lot."
  [& args]
  (println "Hello, World!"))

;;;; connection info for various apps
;; To make work, add the following system property arguments on system startup.
;;  -Dcom.sun.management.jmxremote
;;  -Dcom.sun.management.jmxremote.port=3000
;;  -Dcom.sun.management.jmxremote.authenticate=false
;;  -Dcom.sun.management.jmxremote.ssl=false
(def pmdev {:host "localhost" :port 3000})

;; get all mbean names
(jmx/mbean-names "*:*")

;; get all memory pool mbean names
(jmx/mbean-names "java.lang:type=MemoryPool,name=*")

;; get all garbage collector mbean names
(jmx/mbean-names "java.lang:type=GarbageCollector,name=*")

;; attribute names of a memory pool
(jmx/attribute-names (first (jmx/mbean-names "java.lang:type=MemoryPool,name=*")))

;; attribute names of a garbage collector
(jmx/attribute-names (first (jmx/mbean-names "java.lang:type=GarbageCollector,name=*")))

;; info about all memory pools
(map jmx/mbean (jmx/mbean-names "java.lang:type=MemoryPool,name=*"))

;; info about all garbage collectors
(map jmx/mbean (jmx/mbean-names "java.lang:type=GarbageCollector,name=*"))

(defn pool-names
  "Returns the names of memory pool mbeans. Optional connection info retrieves mbean names from remote jvm."
  ([]
    (jmx/mbean-names "java.lang:type=MemoryPool,name=*"))
  ([conn-info]
    (jmx/with-connection conn-info
      (jmx/mbean-names "java.lang:type=MemoryPool,name=*"))))

(defn pools
  "Returns mbean info for memory pools. Optional connection info retrives info for remote jvm."
  ([]
    (map jmx/mbean (jmx/mbean-names "java.lang:type=MemoryPool,name=*")))
  ([conn-info]
    (jmx/with-connection conn-info
      (doall
        (map jmx/mbean (jmx/mbean-names "java.lang:type=MemoryPool,name=*"))))))

(defn gc-names
  "Returns the names of garbage collector mbeans. Optional connection info retrieves mbean names from remote jvm."
  ([]
    (jmx/mbean-names "java.lang:type=GarbageCollector,name=*"))
  ([conn-info]
    (jmx/with-connection conn-info
      (jmx/mbean-names "java.lang:type=GarbageCollector,name=*"))))

(defn gcs
  "Returns mbean info for garbage collectors. Optional connection info retrives info for remote jvm."
  ([]
    (map jmx/mbean (jmx/mbean-names "java.lang:type=GarbageCollector,name=*")))
  ([conn-info]
    (jmx/with-connection conn-info
      (doall
        (map jmx/mbean (jmx/mbean-names "java.lang:type=GarbageCollector,name=*"))))))

(reflect (first (java.lang.management.ManagementFactory/getGarbageCollectorMXBeans)))

(.isUsageThresholdSupported (first (java.lang.management.ManagementFactory/getMemoryPoolMXBeans)))

;; stuff to run in repl after loading
; (in-ns 'gcmon.core)
; (use 'clojure.pprint)
